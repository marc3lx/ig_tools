
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Follower Bot</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <style type='text/css'>
    #divProgress{
        border:2px solid #ddd; 
        padding:10px; 
        width:321px; 
        height:265px; 
        margin-top: 10px;
        overflow:auto; 
        background:#f5f5f5;
    }

    #progress_wrapper{
        border:2px solid #ddd;
        width:321px; 
        height:20px; 
        overflow:auto; 
        background:#f5f5f5;
        margin-top: 10px;
    }

    #progressor{
        background:#07c; 
        width:0%; 
        height:100%;
        -webkit-transition: all 1s linear;
        -moz-transition: all 1s linear;
        -o-transition: all 1s linear;
        transition: all 1s linear; 

    }
    </style>
  </head>
<body class="container">
<br>
	<div class="col-md-8 col-md-offset-2 well">
		<div class="centered ">
			<br>
				<div class="form-group">
				<input id="btn_fetch" data-loading-text="Please wait..." value="Fetch Users!" class="btn btn-default"/>
				</div>
				<div class="userinput" style="display:none;">
					<form action="" class="form-inline">
					    <div class="form-group">
					    <div class="input-group">
					    	<span class="input-group-addon" id="hashtag-addon1">#</span>
					        <input type="text" id="ui_hashtag" class="form-control" placeholder="Hashtag" aria-describedby="hashtag-addon1">
					        </div>
					    </div>
					    <div class="form-group">    
					         <input type="text" id="ui_count" class="form-control" placeholder="Followers (1-180)">
					    </div> 
         						<input id="btn_submit" value="Submit!" class="btn btn-default"/>
					</form>
				</div>
				<div id="resultmsg"></div>
				<div class="hiding" style="display:none;">
					<div class='float_right'>
	            		<h3>Log</h3>
	            		<div id="divProgress"></div>
	        		</div>
					<div id="progress_wrapper">
		                <div id="progressor"></div>
	            	</div>
            	</div>
			<hr>
				<form action="<?=$_SERVER['PHP_SELF']?>" method="post" class="form-inline"> 
				    <input type="text" name="name" size="20" class="form-control"/> 
				    <input type="submit" value="Get Instagram ID" name="submit" class="btn btn-default"/> 
				    <input type="hidden" value="1" name="sent" /> 
				 
<?php 
session_start();
require './src/Instagram.php';
$test12 = "test";
$_SESSION["s_hashtag"]=$test12;
    if( isset($_POST['sent']) ) 
    { 
    /////// CONFIG ///////
	$username = 'ghu467';
	$password = 'ghu467ghu467';
	$debug = false;
	$user = "ERROR";
	//////////////////////

$i = new Instagram($username, $password, $debug);

try {
    $i->login();
} catch (InstagramException $e) {
    $e->getMessage();
    exit();
}

try {
    $user = $i->searchUsername($_POST['name']);
} catch (Exception $e) {
    $err = $e->getMessage();
    $errb = true;
}
if($errb)
	$userid = $err;
else
	$userid = $user['user']['pk'];

echo "<br>Username ID: <b>$userid</b>";
$errb = false;
} 
?>
				</form>
		</div>
	</div>
<script>    
var submitsuccess;
var followerusers;
var datapath;
var nouser;

$("#btn_fetch").click(function() {
	//ajax_stream();
    var $btn = $(this);
    //$btn.button('loading');
    $('.userinput').slideToggle("slow");
    
});
$("#btn_submit").click(function() {
    var $btn = $(this);
    //$btn.button('loading');
    v_hashtag = $("#ui_hashtag").val();
    v_count = $("#ui_count").val();
    $('.hiding').slideToggle("fast");
    if(v_hashtag == "")
    	v_hashtag = "good";
    ajax_stream();

});
function resetbtn() {
     $("#btn_fetch").button("reset");

     $('.hiding').slideToggle("fast");

     if(submitsuccess && !nouser)
     {
     	$("#resultmsg").text("Successfully fetched " + followerusers + " users. Path: " + datapath);
     }
     else if(!submitsuccess && nouser)
     {
     	$("#resultmsg").text("<b>Error. No user found.");
     }	
     else
     {
     	$("#resultmsg").text("<b>Error while saving.");
     }	
     
}

function ajax_stream(){
    if (!window.XMLHttpRequest){
        alert("Your browser does not support the native XMLHttpRequest object.");
        return;
    }
    try{
        var xhr = new XMLHttpRequest();  
        xhr.previous_text = '';
                         
        xhr.onerror = function() { alert("[XHR] Fatal Error."); };
        xhr.onreadystatechange = function() {
            try{
                if (xhr.readyState == 4){
                    alert('[XHR] Done')
                    resetbtn();
                } 
                else if (xhr.readyState > 2){
                    var new_response = xhr.responseText.substring(xhr.previous_text.length);
                    //alert(new_response);
                    var result = JSON.parse( new_response );

                    
                    document.getElementById("divProgress").innerHTML += result.message + '<br />';
                    document.getElementById('progressor').style.width = result.progress + "%";
                    submitsuccess = result.submitsuccess;
                    nouser = result.nouser;
                    datapath = result.datapath;
                    followerusers = result.followerusers;

                    xhr.previous_text = xhr.responseText;
                }  
            }
            catch (e){
                alert("[XHR STATECHANGE] Exception: " + e);
            }                     
        };
        xhr.open("GET", "write_list.php?v_hashtag=" + v_hashtag + "&v_count=" + v_count, true);
        xhr.send();      
    }
    catch (e){
        alert("[XHR REQUEST] Exception: " + e);
    }
}
</script>
</body>
</html>